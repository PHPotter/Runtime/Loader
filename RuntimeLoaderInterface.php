<?php

namespace PHPotter\Runtime\Loader;

use \PHPotter\Autoloader\SPL\SPLAutoloaderInterface;

/**
 * Interface RuntimeLoaderInterface
 * 
 * @author Jay Potter
 * @package PHPotter\Runtime
 * @subpackage Loader
 */
interface RuntimeLoaderInterface extends SPLAutoloaderInterface {
    
}